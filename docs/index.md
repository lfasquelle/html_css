# HTML, CSS



Deux langages permettent d'écrire les pages html (partie statique):


+ HTML (HyperText Markup Language: langage de balisage d’hypertexte)
+ CSS (Cascading Style Sheets: feuilles de style en cascade)


Nous ne verrons que les bases de ces deux langages. Vous devrez maîtriser ces bases, 
elles vous permettront d'avoir des points de repère pour aller plus loin.
Cela vous permettra de produire vos premières pages html.

Vous aurez par exemple des petits sites à écrire (avec certaines contraintes qui vous seront précisées).
L'écriture de ces petits sites:

+ doit vous permettre de vérifier que vous avez bien enregistré les données des pages qui suivent.
+ vous aménera à faire de la recherche sur le web pour compléter les informations données dans ces pages.

Les pages qui suivent sont  assez denses, mais l'écriture de pages html ne sera pas exigée sans 
documentation: vous devez vous attacher à comprendre et retenir l'essentiel pour être 
capable de démarrer facilement l'écriture d'un  site de quelques pages. Pratiquez  (faîtes et refaîtes
les exercices proposés). 


 
