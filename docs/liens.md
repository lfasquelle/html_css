# Liens

De quoi se balader sur le net.  
Personne ne connaît tous les paramètres html/css par cœur, 
il faut savoir se référer à la documentation régulièrement, ne jamais hésiter à rechercher de l'aide sur internet. 

## Des éléments partiels mais en général suffisants pour votre travail

- [pages de cours html et css](../../ressourcesHTMLCSS/plan.html)
 

## Des liens sur le web
- [http://zonecss.free.fr/](http://zonecss.free.fr/)
- [http://css.mammouthland.net/](http://css.mammouthland.net/)
- [http://www.w3schools.com/css/](http://www.w3schools.com/css/)
- [https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3)
- [http://css.developpez.com/cours/](http://css.developpez.com/cours/)
- [https://css-tricks.com/snippets/css/](https://css-tricks.com/snippets/css/)

