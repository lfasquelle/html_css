# Les éléments de type block



Une page html est découpée en "blocs". Chaque bloc est un rectangle occupant une certaine zone dans la page.

Les deux principaux types de blocs sont:

+ les blocs "inline".
+ les blocs "block".


## Les éléments en `display: block;`


Par défaut, un block occupe toute la largeur de son conteneur et tout bloc débutant 
après la fermeture de ce bloc de type block débutera sur une nouvelle ligne.

Exemples d'éléments de type block par défaut:

+ Élément délimité par les balises `<div>` et `</div>`. 
Un élément div est un élément de type block "générique": aucune sémantique n'est associée à cette balise. 
On utilise donc cette balise à chaque fois que les balises "sémantiques" de type block ne conviennent pas.
+ Élément délimité par les balises `<section>` et `</section>`.
+ Élément délimité par les balises `<p>` et `</p>` pour délimiter un paragraphe.
+ Élément délimité par les balises `<h1>` et `</h1>` pour délimiter un titre (et de même pour h2, h3, h4, h5, h6).
+ Élément délimité par les balises `<article>` et `</article>`.


## Les éléments  en `display: inline;`
 
Un élément de type inline ne commence pas sur une nouvelle ligne 
(mais s'inscrit à la suite de la ligne en cours) 
et n'occupe pas toute la largeur de l'élément parent: 
il n'occupe que la largeur nécessaire à l'affichage de son contenu.

Exemples d'éléments de type inline par défaut :

+ Elément délimité par les balises `<span>` et `</span>`. 
Un élément span est un élément de type inline "générique": aucune sémantique n'est associée à cette balise.
+ Elément `img` pour embarquer une image dans la page.
+ Elément `ancre` délimité par les balises `<a>` et `</a>`.
 
Un élément de type inline ne peut pas contenir un élément de type block.



??? example "Un exemple"

    Le code suivant:

    ```html
    <!DOCTYPE html>
     
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title> block, inline  </title>
            <style>
            span{ color: pink;}
            </style>
        </head>
     
    <body>
     
        <h1>Un titre est par défaut en display: block</h1>


        <article>
            <h2>Et un article?</h2>
            <p>Un article, un paragraphe sont par défaut display: block. </p>
            <p> Un <span>span</span> est un élément en ligne, comme une ancre.</p>
        </article>
    </body>

    </html>
    ```

    aura dans la plupart des navigateurs à peu près le rendu suivant:

    <iframe src="fichiersHTML/block.html" 
         width="100%" 
         height="300" 
         style="border:2px solid orange">
    </iframe> 




## Les blocs paragraphes

La balise `<p></p>` délimite des paragraphes. 
Un élément  `p` est  de type block: si je ferme un bloc p et en commence
un autre juste après, le second commence sur une nouvelle ligne.


## Les blocs div

La balise générique pour les blocs de type block est la balise `<div></div>`.

Un bloc `<div></div>` peut contenir des blocs `<p></p>`, mais des blocs `<p></p>` ne doivent jamais
contenir d'autres blocs de type block.

??? example "Un exemple"

    Tester l'exemple ci-dessous.
    Pour cela:

    - ouvrir un éditeur de texte (par exemple geany),
    - copier le code ci-dessous,
    - nommer le fichier sous la forme nom.html,
    - ouvrir enfin ce fichier avec un navigateur,
    - observez !


    ```html
    <!DOCTYPE html>
    <html lang="fr">

     
    <head>
        <meta charset="utf-8">
        <title> je suis le titre  </title>
        
     
        <style>
        /* code css ajoutant des cadres pour visualiser les blocs */
        body{ /* pour que nos blocs n'occupent que 70% partie de la largeur d'écran */
            width: 70%;
            margin:0 auto;
        }
        div{
            border: 2px solid green; /* les div seront encadrés en vert */
            margin-bottom: 5px; /* 5 pixels de marge après un div */
            padding: 3px; /* un peu de marge entre le texte et la bordure.*/
        }
        p{
            border: 1px dashed red; /* les paragraphes seront encadrés en rouge */
        }
        </style>
    </head>
     
    <body>
        
    <h1>Des citations</h1>
        
    <div>
        <h2>Du Raymond devos</h2>

        <p>Je connais un critique qui est en même temps auteur... ce qui le met en tant qu'auteur dans une situation critique !</p>

        <p>Une fois rien, c'est rien ; deux fois rien, ce n'est pas beaucoup, mais pour trois fois rien, on peut déjà s'acheter quelque chose, et pour pas cher. </p>

        <p>Mon pied droit est jaloux de mon pied gauche. Quand l'un avance, l'autre veut le dépasser. Et moi, comme un imbécile, je marche!</p>
    </div>



    <div>
        <h2>Du Philippe Geluck</h2>

        <p>Boire du café empêche de dormir. Par contre, dormir empêche de boire du café.</p>

        <p>Il est plus facile de jouer au mikado avec des spaghettis crus qu'avec des spaghettis cuits.</p>

        <p>Les imbéciles pensent que tous les noirs se ressemblent. Je connais un noir 
        qui trouve, lui, que tous les imbéciles se ressemblent.</p>
    </div>
     
    </body>
    </html>
    ```



## Les titres 

Nous avons vu dans l'exemple ci-dessus deux balises de titre `<h1></h1>` et `<h2></h2>`.

Il existe 6 niveaux de titre.

+ `<h1></h1>`
+ `<h2></h2>`
+ `<h3></h3>`
+ `<h4></h4>`
+ `<h5></h5>`
+ `<h6></h6>`

Ces éléments sont  de type block par défaut.


??? example  "Un exemple"

    Le code suivant:

    ```html
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>   article </title>
            <style>
            h1{
                text-align: center; /* centre le texte dans l'élément de titre*/
                font-size:70px;/* taille de police */
                color: #E0B0FF; 
            }
            article{
                border: 1px dashed gray; /* bordures en gris tiretées, 1px d'épaisseur*/
                padding: 2px;/* marges intérieures*/
                margin: 3px;/*marges extérieures*/
            }
            </style>
        </head>
        
    <body> 

    <h1> La guerre des étoiles </h1>

    <p> Extrait d'un  article wikipedia </p>

    <article>
    <h2> Épisode IV </h2>
    <p>Le croiseur de Dark Vador capture le vaisseau de la princesse Leia qui détient les plans volés de l'Étoile Noire, l'arme absolue du puissant Empire galactique. <br>
    La princesse dissimule les plans dans la mémoire du droïde R2-D2 qui s’éjecte sur la planète Tatooine en compagnie de son alter ego C-3PO. </p>
    <p>Les droïdes sont capturés par des ferrailleurs Jawas avant d'être finalement vendus à la ferme d'Owen Lars où vit Luke Skywalker, un jeune fermier qui rêve d'aventures. Ce dernier intercepte un message de la princesse Leia qui cherche de l'aide d'un certain Obi-Wan Kenobi. </p>
    </article> 

    <article>
    <h2> Épisode  V </h2>
    <p>L'Empire attaque la base rebelle de la planète Hoth.</p>
    <p>Dans la débandade qui s'ensuit, les héros se retrouvent séparés. Leia, Chewbacca, Solo et C-3PO doivent en effet faire face à une panne de leur vaisseau et se réfugient chez un ancien ami du contrebandier, Lando Calrissian, sur la planète Bespin. </p>
    </article> 

    <article>
    <h2> Épisode  VI </h2>
    <p>Le début de l'épisode VI présente le plan mis au point par les héros pour secourir Han Solo des mains de Jabba le Hutt. Il se révèle être un succès et se solde également par la mort du Hutt.</p>
    <p> Dans le même temps, l'Empire met au point une deuxième station de combat, l'Étoile de la Mort, dont la construction est supervisée par l'Empereur Palpatine lui-même. L'Alliance rebelle voit donc là une occasion de frapper un coup décisif pour la victoire, d'autant que la station semble vulnérable. </p>
    </article>  
           
    </body>
    </html>
    ```

    donne par défaut le résultat suivant (dans la plupart des navigateurs):


    <iframe src="fichiersHTML/titres.html" 
         width="100%" 
         height="500" 
         style="border:2px solid orange">
    </iframe> 
