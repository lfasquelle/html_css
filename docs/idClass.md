# Identifiant, classe




Pour appliquer des règles de style css aux éléments d'une page .html, 
il faut pouvoir sélectionner ces éléments (et uniquement ceux que l'on veut styler). 
Il existe pour cela différentes sortes de sélecteurs. Voici une description des sélecteurs les plus utilisés. 




## Sélecteurs d'éléments



Une règle css telle que : 

```html
p{
	color: darkblue;
}
```
concernera tous les éléments de type paragraphe.
On peut ainsi facilement définir des comportements «par défaut» pour les éléments essentiels d'une page.




## Sélecteurs de classe


Il peut exister plusieurs types de paragraphes dans une page web. Pour les distinguer, il faut définir des groupes de balises à l'aide de l'attribut class.
Voici comment écrire en vert des paragraphes correspondants aux énoncés d'exercices et en rouge le paragraphes correspondants aux corrections: 

```html
<!-- Partie html -->
<p class="enonce">
	Un énoncé d'exercice.
</p>
<p class="correction">
	Une correction d'exercice
</p>
```

```css
/* Partie css */
.enonce{				/* ne pas oublier le "." devant le nom de la classe */
	color: darkgreen;
	}
.correction{				/* ne pas oublier le "." devant le nom de la classe */
	color: red;
	}
```

## Sélecteurs d'identifiant
On peut aussi désigner un élément unique dans une page avec l'attribut id. Cet élément sera unique dans la page, mais il sera généralement présent dans chaque page du site.

Voici comment écrire en rouge le paragraphe de conclusion de la page : 
```html
<!-- Partie html -->
<p id="conclusion">
	Conclusion de la page.
</p>
```

```css
/* Partie css */
#conclusion{			/* ne pas oublier le "#" devant le nom de l'identifiant */
	color: red;
	}
```
