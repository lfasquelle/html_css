# HTML


## L'abréviation HTML

HTML sont les initiales de "HyperText Markup Language" que l'on traduira 
par "langage de balisage hypertexte".


Dans ce nom, deux notions:

+ la notion de balise.
+ la notion d'hypertexte.



## Hypertexte.

### Hypertexte.

Une définition ([wikipedia](https://fr.wikipedia.org/wiki/Hypertexte)):

Un hypertexte est un document 
ou un ensemble de documents contenant des unités d'information liées entre elles par des hyperliens. 

### Hyperlien

Une définition d'hyperlien ([wikipedia](https://fr.wikipedia.org/wiki/Hyperlien)):

Un hyperlien ou lien hypertexte ou lien web ou simplement lien, est une référence dans un système hypertexte 
permettant de passer automatiquement d'un document consulté à un document lié. 
Les hyperliens sont notamment utilisés dans le World Wide Web 
pour permettre le passage d'une page Web à une autre à l'aide d'un clic. 



## Balise

Une balise, sur la route, est une signalisation qui indique un danger, une route à suivre...
Dans les langages à balises, les balises (ou *marques*) servent à délimiter une portion de texte afin
de lui donner une signification particulière.

Par exemple, en HTML, on délimitera un paragraphe à l'aide des balises `<p>`  (balise ouvrant le paragraphe)
et `</p>` (balise fermant le paragraphe).

!!! note
    Il existe de nombreux langages à balises. Par exemple:
    
    + XML
    + $\LaTeX$

