# Liens



Pour la plupart des chapitres dans l'année, tout le contenu du cours devra être parfaitement maîtrisé.

Ce cours html, css fait exception. Essayez de retenir le maximum d'informations mais il ne sera pas exigé 
d'écrire un site sans documentation. 
 
Il faut savoir se référer à la documentation régulièrement, ne jamais hésiter à rechercher de l'aide sur internet. 




## Cours et exercices

Vous avez déjà travaillé quelques-unes des pages de cours et exercices de ces
 [pages de cours html et css](https://fasquelle.ovh/nsi/ressourcesHTMLCSS/plan.html). N'hésitez pas à poursuivre, à traiter d'autres 
 exercices de ces pages.
 
Si vous voulez que je développe un peu plus l'une de ces pages (plus d'exemples, d'exercices) ou que j'ajoute
un thème, n'hésitez pas à me le demander.
 

## Des liens sur le web

On trouve aujourd'hui, sur la toile, de nombreux tutoriels ou sites de
documentation pour se former à   HTML et CSS.
N'hésitez pas à les consulter pour approfondir.

En voici quelques-uns:


- [tuto et infos html MDN](https://developer.mozilla.org/fr/docs/Web/HTML)
- [tuto et infos CSS MDN](https://developer.mozilla.org/fr/docs/Web/CSS)
- [https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3](https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3)
- [http://zonecss.free.fr/](http://zonecss.free.fr/)
- [http://css.mammouthland.net/](http://css.mammouthland.net/)
- [http://www.w3schools.com/css/](http://www.w3schools.com/css/)
- [http://css.developpez.com/cours/](http://css.developpez.com/cours/)
- [https://css-tricks.com/snippets/css/](https://css-tricks.com/snippets/css/)

