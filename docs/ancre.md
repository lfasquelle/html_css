# Les liens hypertexte


Ce qui a fait le succès du web est incontestablement le principe des liens hypertextes.

On explique ci-dessous les principes de ces liens et comment les écrire dans une page html.

## Cours

Travailler [cette page de cours](https://fasquelle.ovh/nsi/ressourcesHTMLCSS/cours/htmlCss/lien/c1.html).


## Exercices 


Travailler [ces exercices](https://fasquelle.ovh/nsi/ressourcesHTMLCSS/cours/htmlCss/lien/ex1.html).


