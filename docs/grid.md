# Grille CSS



La propriété css `grid` est une propriété permettant de placer dans la page assez facilement les éléments HTML.

!!! important
    D'autres propriétés css sont utiles à la mise en page, mais nous ne verrons que grid.
    Si vous voulez en savoir plus sur ces autres propriétés, chercher sur la toile, notamment à partir des
    mots-clefs suivants:
    
    + [flexbox](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Flexible_Box_Layout/Concepts_de_base_flexbox)
    + [float](https://developer.mozilla.org/fr/docs/Web/CSS/float)
    + [display](https://developer.mozilla.org/fr/docs/Web/CSS/display)


## grid 


 
+ Tester le code suivant.
+ Modifier les valeurs `1fr 3fr;` qui apparaissent.


```html
<!DOCTYPE html>
<html lang="fr">
 
<head>
	<meta charset="utf-8">
	<title> grid  </title>
	<style>
        body{
            width:60%;
            margin:0 auto;
        }
        div{ padding: 4px;}
        div.conteneur{
             border: 3px solid black;
             display: grid;
             grid-template-columns: 1fr 3fr;
         }
         
        div.item{
             border: 2px dashed pink;
         }
    </style>
</head>
 
<body>
    
<div class="conteneur">
    <div class="item">
        Case 1
    </div> 
    <div class="item">
        Case 2
        <p class="sousitem"> bla bla</p>
    </div>
    <div class="item">
        Case 3
    </div>
</div>
 
</body>
</html>
```

+ L'élément div de class nommée conteneur est déclaré en `display: grid;` : ces fils directs (c'est à dire
ici les éléments div de classe nommée item) seront
affichés suivant un principe de grille, ils constitueront les cellules de la grille. Les descendants suivants
par contre (ici par exemple les éléments de type p, petits-fils du div conteneur) se placent
dans la cellule.
+ Pour l'élément div de classe conteneur, on a déclaré `grid-template-columns: 1fr 3fr;`. Cela signifie
que chaque ligne de la grille sera constituée de deux cellules, la première occupant 1/(1+3) = 25% de la largeur
du conteneur, la seconde occupant les 75% restant. Si le div conteneur  a par exemple 6 fils directs, on verra 
3 lignes de deux cellules.


## Une mise en page classique

Voici un exemple simple d'utilisation de grid pour une mise en page classique.

```html
<!DOCTYPE html>
<html lang="fr">
	
<head>
<meta charset="utf-8">
<title> grid  </title>
<style>
	
	header{
		text-align: center;
		background-color: rgb(255,255,0);
	}
	footer{
		text-align: center;
		background-color: rgb(0,255,255);
	}
	
	
	.conteneur{ /* contiendra les éléments à disposer en "grille" */
		display: grid;
		grid-template-columns: 20% 80%;
		margin-bottom: 1em; /* marge sous la boîte */
		
	}
	nav{
		border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
	main{
		border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
 
</style>
</head>
    
<body>
	
	<header>
		<h1>Titre de page</h1>
		<h2>sous-titre</h2>
	</header>
	
	
	<div class="conteneur">
		
		<nav>
			liens de navigation du site
		</nav>
		
		<main>
			<section>
				section 1
			</section>
			
			<section>
				section 2
			</section>
		</main>
		
	</div><!-- fin de la partie "grille" -->
	
	
	<footer>
		Éléments de pied de page
	</footer>
	  
</body>

</html>
```

Copier-coller ce code dans un fichier .html et étudier le résultat en ouvrant la page dans un navigateur.
Faites quelques modifications pour comprendre.


## Emboîter des grilles

Une cellule de grille peut elle-même être une grille. Visualiser par exemple le résultat de la page suivante:


```html
<!DOCTYPE html>
<html lang="fr">
	
<head>
<meta charset="utf-8">
<title> grid  </title>
<style>
	
	header{
		text-align: center;
		background-color: rgb(255,255,0);
	}
	
	
	footer{
		text-align: center;
		background-color: rgb(0,255,255);
	}
	
	
	.conteneur{ /* contiendra les éléments à disposer en "grille" */
		display: grid;
		grid-template-columns: 1fr 6fr;
		margin-bottom: 1em; /* marge sous la boîte */
	}
	
	nav{
		 
		border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
	main{
		 
		border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
	
	
	.grilleContenu{ /* une  grille à l'intérieur de la première grille pour la partie main */
		display: grid;
		grid-template-columns: 50% 50%;
		grid-template-rows: 100px 100px;
	}
	
	.no{
		background-color: cornsilk;
	}
	.ne{
		background-color: lavender;
	}
	.se{
		background-color: khaki;
	}
	.so{
		background-color:WhiteSmoke;
	}
 
</style>
</head>
    
<body>
	
	<header>
		<h1>Titre de page</h1>
		<h2>sous-titre</h2>
	</header>
	
	
	<div class="conteneur">
		
		<nav>
			liens de navigation du site
		</nav>
		
		<main class="grilleContenu">
          
			<section class="no">
				Ligne 1, colonne 1
			</section>
			
			<section class="ne">
				Ligne 1, colonne 2
			</section>
          
          	<section class="so">
				Ligne 2, colonne 1
			</section>
          
          	<section class="se">
				Ligne 2, colonne 2
			</section>
          
		</main>
		
	</div><!-- fin de la partie "grille" -->
	
	
	<footer>
		Éléments de pied de page  
	</footer>
	  
</body>

</html>
```


## Nommer les emplacements

Pour affiner le placement des fils dans la grille, on peut nommer les emplacements.
Le nom de l'emplacement se retrouvera dans le css du fils.
Si plusieurs emplacements (sur une forme rectangulaire) portent la même lettre, un même fils 
accupera plusieurs cellules.

Exemple -- Visualiser le rendu du code ci-dessous.

```html
<!DOCTYPE html>
<html lang="fr">
	
<head>
<meta charset="utf-8">
<title> grid  </title>
<style>
	
	header{
		text-align: center;
		background-color: rgb(255,255,0);
	}
	
	
	footer{
		text-align: center;
		background-color: rgb(0,255,255);
	}
	
	
	.conteneur{ /* contiendra les éléments nav et main en "grille" */
		display: grid;
		grid-template-columns: 20% 80%;
		margin-bottom: 1em; /* marge sous la boîte */
		grid-template-areas: "gauche droite";
	}
	
	nav{
		grid-area: gauche; /* placement en colonne 1 */
		border: 1px dashed red; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
	main{
		grid-area: droite; /* placement en colonne 2 */
		border: 1px solid green; /* bordure pour mieux situer l'élément dans cet exemple */
		padding: 1em;
	}
	
	
	.grilleContenu{ /* la partie main a ses fils disposés en grille également. */
		display: grid;
		grid-template-columns: 1fr 1fr 1fr;
		grid-template-areas: "T T T"   
          					 "A B B"
							 "A C D"; /* structure de la grille */
		grid-gap: 2px;
	}
  
   .monTitre{ 
     grid-area: T; /* l'élément de class monTitre occupera les cellules T */
     background-color: yellow;
     text-align: center;
   }
	
	.no{ 
		grid-area: A; /* l'élément de class no occupera les cellules marquées A */
		background-color: green;
		color:orange;
	}
	.ne{
		grid-area: B; 
		background-color: lavender;
	}
	 
	.so{
		grid-area: C; 
		background-color: orange;
	}
    
	.se{
		grid-area: D; 
		background-color: khaki;
	}
</style>
</head>
    
<body>
	
	<header>
		<h1>Titre de page</h1>
		
	</header>
	
	
	<div class="conteneur">
		
		<nav>
			liens de navigation du site
		</nav>
		
		<main class="grilleContenu">
          
            <h2 class="monTitre">Le coin Geluck</h2>
			<section class="no">
				Boire du café empêche de dormir. Et dormir empêche de boire du café. 
			</section>
			
			<section class="se">
				Il est plus facile de jouer au mikado avec des spaghettis crus qu'avec des spaghettis cuits.
			</section>
			
			<section class="so">
				Quand je dis que ma richesse est intérieure je veux dire que mon argent est dans un coffre.
			</section>
			
			<section class="ne">
				Les imbéciles pensent que tous les noirs se ressemblent. Je connais un noir qui trouve, lui, que tous les imbéciles se ressemblent.
			</section>
		</main>
		
	</div><!-- fin de la partie "grille" -->
	
	
	<footer>
		Éléments de pied de page
	</footer>
	  
</body>

</html>
```

##  Compléments sur grid

 
Pour en savoir plus sur les possiblités de grid:


+ [alsacreation](https://www.alsacreations.com/article/lire/1388-css3-grid-layout.html)
+ [MDN](https://developer.mozilla.org/fr/docs/Web/CSS/CSS_Grid_Layout)
+ [w3schools](https://www.w3schools.com/css/css_grid.asp)
+ [developpez.com](https://css.developpez.com/tutoriel/se-lancer-dans-css-grid/)
+ [une série d'exemples pouvant constituer un modèle pour une mise en page](https://gridbyexample.com/examples/)
    
    
    
## Exercice


Ci-dessous un code html de page:

```html
<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="utf-8">
        <title> grid </title>
        <style>
	  
        </style>
    </head>
    
<body>

<header>
	<h1>Un super titre</h1>
	<h2> Un joli sous-titre</h2>
</header>


<div class="grillePrincipale">
	
	<nav>
		Ici on navigue.
		<ul>
			<li><a href="#">lien 1</a></li>
			<li><a href="#">lien 2</a></li>
			<li><a href="#">lien 3</a></li>
			<li><a href="#">lien 4</a></li>
		</ul>
	</nav>
	
	<main>
        
        <article>
            Raymond Devos
        </article>
        
        <article>
            L'autre jour, au café, je commande un demi. J'en bois la moitié. Il ne m'en restait plus.
        </article>
        
        <article>
            Je suis adroit de la main gauche et je suis gauche de la main droite.
        </article>
        
        <article>   
            Se coucher tard nuit.
        </article>
        
        <article>
            Quand un homme ne dit rien alors que tout le monde parle, on n'entend plus que lui !
        </article>
        
        <article>
            Si l'on peut trouver moins que rien, c'est que rien vaut déjà quelque chose.
        </article>
        
        <article>
            Ne rien faire, ça peut se dire. Ca ne peut pas se faire !
        </article>
            
    </main>
	
	<aside>
		Ici, plein de remarques complémentaires super captivantes.
	</aside>
</div>

<footer>
C'est le pied !
</footer>

</body>         
</html>
```

Votre mission est de compléter le contenu des balises `style` de façon à obtenir une mise en page similaire
à la suivante: 

![](images/misenpageGrid.png)



??? solution "Un code possible"

    On propose un code. 
    
    Nous avons utilisé, pour les couleurs de fond des articles, des sélecteurs dont nous n'avons pas parlé auparavant.
    Vous pouvez bien sûr vous en passer en ciblant une à une les cellules.
    
    
    ```html
    <!DOCTYPE html>
    <html lang="fr">

        <head>
            <meta charset="utf-8">
            <title> grid </title>
            <style>
            html{
                font-size: 20px;
            }
            header{ 
                background-color: #999999;
                text-align: center;
                color: white;
                font-weight: bold;
            }
            footer{ 
                background-color: #999999;
                text-align: center;
                color: white;
                font-weight: bold;
                margin-top: 10px;
            }
          
            h1{
                font-size: 2rem;
            }
            h2{
                font-size: 1.5rem;
            }
            h3{
                font-size: 1.2rem;
            }
            
            nav{
                background-color: #bbbbbb;
            }
            
            aside{
                background-color: green;
                color: white;
                font-weight: bold;
            }
            
            .grillePrincipale{
                display: grid;
                grid-template-columns: 200px 1fr 200px;
                column-gap: 5px;
            }
            
            main{
                display: grid;
                grid-template-columns: repeat(2, 1fr);
                gap: 5px;
            }
            
            main article:nth-child(4n+0) { /* article de rang multiple de 4 en tant que fils de main */
                background-color: pink;
            }
            main article:nth-child(4n+1) {/* article de rang 1 + multiple de 4 en tant que fils de main */
                background-color: GreenYellow;
            }
            main article:nth-child(4n+2) {
                background-color: Cornsilk;
            }
            main article:nth-child(4n+3) {
                background-color: #00FFFF;
            }
            a{
                text-decoration: none;
                color: white;
                font-weight:bold;
            }
            </style>
        </head>
        
    <body>

    <header>
        <h1>Un super titre</h1>
        <h2> Un joli sous-titre</h2>
    </header>


    <div class="grillePrincipale">
        
        <nav>
            Ici on navigue.
            <ul>
                <li><a href="#">lien 1</a></li>
                <li><a href="#">lien 2</a></li>
                <li><a href="#">lien 3</a></li>
                <li><a href="#">lien 4</a></li>
            </ul>
        </nav>
        
        <main>
        
            <article>
                Raymond Devos
            </article>
            
            <article>
                L'autre jour, au café, je commande un demi. J'en bois la moitié. Il ne m'en restait plus.
            </article>
            
            <article>
                Je suis adroit de la main gauche et je suis gauche de la main droite.
            </article>
            
            <article>   
                Se coucher tard nuit.
            </article>
            
            <article>
                Quand un homme ne dit rien alors que tout le monde parle, on n'entend plus que lui !
            </article>
            
            <article>
                Si l'on peut trouver moins que rien, c'est que rien vaut déjà quelque chose.
            </article>
            
            <article>
                Ne rien faire, ça peut se dire. Ca ne peut pas se faire !
            </article>
            
        </main>
        
        <aside>
            Ici, plein de remarques complémentaires super captivantes.
        </aside>
    </div>

    <footer>
    C'est le pied !
    </footer>

    </body>         
    </html>
    ```
