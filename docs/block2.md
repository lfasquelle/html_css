# Quelques éléments de structure



Quelques éléments (en général de type block) permettent de mieux structurer de façon logique
le code d'une page.


!!! important
    Rappeler vous toujours que le code html ne met jamais rien en forme. Il a uniquement  un rôle
    de découpage logique du document.  
    
    
##  &Eacute;lément section


Nous avons vu les éléments de type block `<div>` et `<p>`. Il existe aussi les éléments `<section>`
dont on se sert en général pour découper la page en groupes de contenus thématiques. Une section commence en général
par un titre (balise h2 par exemple, la balise h1 étant réservée par le titre principal de la page).



+ [Exemple sur W3 schools](https://www.w3schools.com/TAgs/tryit.asp?filename=tryhtml5_section)
+ [Exemple sur MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Element/section)


??? example "Un exemple"

    Le code suivant:
    
    ```html
    <!DOCTYPE html>
    <html lang="fr">
    
        <head>
            <meta charset="utf-8">
            <title> section, article  </title>
            <style>
            h1{
                text-align: center; /* centre le texte dans l'élément de titre*/
                font-size:70px;/* taille de police */
                color: #E0B0FF; 
            }
            article{
                border: 1px dashed gray; /* bordures en gris tiretée, 1px d'épaisseur*/
                padding: 2px;/* marges intérieures*/
                margin: 3px;/*marges extérieures*/
                width: 80%;
            }
            section{
                border: 1px dotted orange; /* bordures en orange pointillées, 1px d'épaisseur*/
                padding: 2px;/* marges intérieures*/
                margin: 10px;/*marges extérieures*/
            }
            </style>
        </head>
        
    <body> 

    <h1> La guerre des étoiles </h1>

     
    <section>

    <h2> La trilogie d'origine  1977-1983</h2>

        <article>
        <h3> Épisode IV </h3>
            blabla
        </article> 

        <article>
        <h3> Épisode  V </h3>
            blabla
        </article> 

        <article>
        <h3> Épisode  VI </h3>
            blabla
        </article> 

    </section> 




    <section>

    <h2> La Prélogie 1999-2005</h2>

        <article>
        <h3> Épisode I </h3>
            blabla
        </article> 

        <article>
        <h3> Épisode  II </h3>
            blabla
        </article> 

        <article>
        <h3> Épisode  III </h3>
            blabla
        </article> 

    </section> 




    <section>

    <h2> La troisième trilogie 2015-2019</h2>

        <article>
        <h3> Le réveil de la Force </h3>
            blabla
        </article> 

        <article>
        <h3> Les Derniers Jedi </h3>
            blabla
        </article> 

    </section> 
           
    </body>
    </html>
    ```
    
    donnera [ce rendu](fichiersHTML/section.html).
    
    <iframe src="fichiersHTML/section.html" 
     width="100%" 
     height="500" 
     style="border:2px solid orange">
    </iframe> 

## &Eacute;lément header



L'élément HTML `<header>` délimite un contenu introductif (notamment un titre) et/ou des éléments de navigation. 

Il peut y avoir un élément header pour l'ensemble de la page.




+ [Exemple sur W3 schools](https://www.w3schools.com/tags/tag_header.asp)
+ [Exemple sur MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Element/header)




!!! important
    Ne confondez pas les balises `head` (destinée à recevoir les métainformations de la page)
    et `header` que l'on vient de décrire.
    
    

## &Eacute;lément footer


De façon "symétrique", l'élément HTML `<footer>` délimite le pied de page 
(ou un pied de section par exemple). 
Un footer   contient par exemple des informations sur l'auteur.
 


+ [Exemple sur MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Element/footer)
+ [Exemple sur W3 schools](https://www.w3schools.com/TAgs/tag_footer.asp)



## Exercice


Copier et coller le code ci-dessous dans un fichier .html.

??? note "Le code"

    ```html
    <!DOCTYPE html>
    <html lang="fr">

        <head>
            <meta charset="utf-8">
            <title> structure 1 </title>
            <style>
                *{
                    margin: 0;  /* marge  0 par défaut pour tous les éléments */
                    padding: 0.5em; /* marge interne par défaut de 0.5em pour tous les éléments. */
                }
                header{
                    background-color: #ed8683;  /* couleur de fond du header */
                    margin-bottom: 1em; /* marge en dessous de l'élément */
                }
                main{
                    background-color: #a8b8c5; /* couleur de fond du main */
                }
                footer{
                    background-color: #ffff66; /* couleur de fond du footer */
                    margin-top: 1em; /* marge au dessus de l'élément */
                }
                section{
                    background-color: #B0E4BE;  /* couleur de fond des sections */
                    width: 80%; /* largeur */ 
                    margin: 1em auto 1em auto; /* marges */
               }
            </style>
        </head>
        
        <body>
               
                     Titre de la page  
                 
                     page 1 
                     page 2 
                     page 3 
                
              
              
                     Titre de la section 1 
                     Premier paragraphe de la section 1  
                     Second paragraphe de la section 1 
                  
                  
                  
                      Titre de la section 2  
                      Premier paragraphe de la section 2  
                      Second paragraphe de la section 2  
                      Troisième paragraphe de la section 2  
                 
              
              
                pied de page, liens légaux, contact
            
               
             
        </body>
    </html>
    ```



L'objectif est d'ajouter les balises nécessaires pour obtenir le rendu ci-dessous.
Pour le choix des balises, vous vous appuyerez sur le rendu attendu et les déclarations css: à vous de faire
la correspondance.


??? note "Rendu recherché"

    ![](images/exerciceBlock.png)
    
    
??? solution "Un code possible"

    [Ce fichier](fichiersHTML/exerciceBlock.html) présente un code possible (vous obtiendrez 
    le code par un clic droit/code source).
    
    
    ```html
    <!DOCTYPE html>
    <html lang="fr">

        <head>
            <meta charset="utf-8">
            <title> structure 1 </title>
            <style>
                  *{
                    margin: 0; 
                    padding: 0.5em;
                }
                header{
                    background-color: #ed8683;  
                    margin-bottom: 1em;
                }
                main{
                    background-color: #a8b8c5;
                }
                footer{
                    background-color: #ffff66;
                    margin-top: 1em;
                }
                section{
                    background-color: #B0E4BE;
                    width: 80%;
                    margin: 1em auto 1em auto;
               }
            </style>
        </head>
        
        <body>
              <header>
                <h1> Titre de la page </h1>
                <nav> 
                    <a href="#"> page 1</a>
                    <a href="#"> page 2</a>
                    <a href="#"> page 3</a>
                </nav>
              </header>
              
              <main>
                  <section>
                    <h2> Titre de la section 1</h2>
                    <p> Premier paragraphe de la section 1 </p>
                    <p> Second paragraphe de la section 1 </p>
                  </section>
                  
                  <section>
                    <h2> Titre de la section 2 </h2>
                    <p> Premier paragraphe de la section 2 </p>
                    <p> Second paragraphe de la section 2 </p>
                    <p> Troisième paragraphe de la section 2 </p>
                  </section>
              </main>
              
              <footer> 
                pied de page, liens légaux, contact
              </footer>
               
             
        </body>
    </html>
    ```
