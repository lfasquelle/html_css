# Structure d'une page HTML



## Le balisage "minimum"


La structure d'une page HTML comporte toujours au moins le code suivant:


```html
<!DOCTYPE html>
 

<html lang="fr">

    <head>
        <meta charset="utf-8">
        <title> Le titre de la page  </title>
    </head>
 
    <body>
     ICI LE CONTENU DE LA PAGE
    </body>
 
</html>
```


Nous expliquons ci-dessous rapidement le rôle de chaque partie de ce code.

## Le doctype

La première ligne du document est le doctype :

```html
<!doctype html>
```

Le mot doctype peut être écrit en majuscules ou en minuscules. 
Il en ira de même pour les balises html, mais nous les écrirons systématiquement en minuscules.

Le rôle du doctype (déclaration de type de document) est de déclarer à votre navigateur 
quel type de document html il va lire. 

En html5, le doctype est simple (c'est celui qui est présenté ci-dessus). 
Mais par exemple en html 4.01 strict, la déclaration de doctype est la suivante :


```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"     "http://www.w3.org/TR/html4/strict.dtd">
```

Le [DTD](https://www.alsacreations.com/article/lire/560-dtd-doctype-html-xhtml-comment-choisir.html) 
(Déclaration de Type de Document, déclaration réalisée par la donnée du doctype comme indiqué ci-dessus) 
sert à indiquer à quelles règles d'écriture obéit le code d'une page HTML. 
Ces règles sont contenues dans les Définitions de Type de Document, 
hébergées sur le site du [W3C](https://fr.wikipedia.org/wiki/World_Wide_Web_Consortium). 
Par exemple, vous trouverez la définition du doctype 4.01 strict 
[sur cette page](https://www.w3.org/TR/html4/sgml/dtd.html). 






## La balise html

La seconde ligne est constituée de la balise ouvrante html


```html
<html>
```

Cette balise est complétée par la **toute dernière ligne** de la page : la balise fermante html

```
</html>
```

Ces deux balises délimitent le code html 
(code écrit en respectant les règles définies par le doctype déclaré en première ligne). 

Dans une page html, pour parler de l'élément html, on parlera souvent de l'élément racine du document.

On ajoutera la langue utilisée dans nos pages comme **attribut** de la balise html:

```html
<html lang="fr">
```
 

!!! note "Rôle de cet attribut lang"
    Un extrait de [cette page alsacreations](https://www.alsacreations.com/astuce/lire/1151-langue-du-contenu.html):
    
    Il est important sur une page web d’indiquer clairement la langue du contenu. 
    Les informations données sur la ou les langues du contenu 
    seront utiles pour les outils d’indexation (moteurs de recherche), 
    les outils de traduction automatique ou encore ceux de synthèse vocale. 
    Par exemple, un lecteur d’écran a besoin de connaitre la langue du contenu 
    pour pouvoir le lire correctement, quand cette langue diffère de ses paramètres par défaut.

## L'entête

L'en-tête du document est délimité par la balise ouvrante head

```html
<head>
```

et la balise fermante correspondante :

```html
</head>
```

L'en-tête sert à donner des renseignements sur le document (des méta-informations). 
 

### title

Le premier renseignement contenu dans l'en-tête est le titre (title) donné à la page.

```html
<title>titre de la page</title>
```

Ce titre ne s'affiche pas dans la page. 
Avec firefox par exemple, il est affiché dans l'onglet correspondant à la page 
(au-dessus de l'espace dans lequel l'adresse de la page est inscrite). 

Vous pouvez repérer sur la page que vous êtes en train de lire le titre : 

```
Structure - Langages html et css
```

Vous pouvez vérifier que "Structure - Langages html et css" est bien ce qui est inscrit entre les balises 
`<title>` et `</title>` du code de cette page en faisant un clic droit dans la page et en sélectionnant 
"code source de la page".


### charset

Un autre renseignement important est l'encodage utilisé pour le document.

On pourra retenir que le rôle essentiel de cette indication est de permettre que certaines lettres,
notamment les lettres accentuées, soient affichées correctement.


```html
<meta charset="utf-8">
```
 

Il est important de remarquer qu'il n'y a pas de balise de fermeture correspondant à celle-ci. 
On parlera de balise autofermante pour ce type de balise. Elles peuvent d'ailleurs être écrites 
comme suit pour insister sur cette autofermeture :


```html
<meta charset="utf-8" />
```

Nous reviendrons sur l'importance de cette information. Lisez
[cette page](https://www.alsacreations.com/astuce/lire/69-declarer-encodage-des-caracteres.html) 
pour savoir l'essentiel sur le rôle de l'encodage.





## Le corps


C'est entre les balises d'ouverture et de femeture `<body>` et `</body>` que l'on inscrira le contenu de la page.

Le contenu de l'en-tête n'est pas visible dans le navigateur, 
seul le contenu du corps (élément body) est visible 
lorsqu'on ouvre la page avec un navigateur.



!!! important
    Dans une page html, il y un seul élément `head`, un seul élément `body`.
