# Les couleurs

Mettre en couleur des éléments HTML ne se fait pas avec du code HTML mais avec du code CSS (le code CSS va permettre
d'agir sur les éléments HTML).





## Le codage RGB

RGB signifie red, green, blue.

Le codage RGB des couleurs consiste à attribuer 

+ une composante de rouge, 
+ une composante de vert, 
+ et enfin une composante de bleu.



Ces composantes sont des entiers entre 0 et 255 (c'est à dire entre 0 et 2<sup>8</sup>-1).
 
 
<table style="text-align:center;">
    <thead>
    <tr>
    <th>Code rgb</th> <th> couleur </th>
    </tr>
    </thead>
    
    <tbody>
    <tr> <td>rgb(255,0,0)</td> 
        <td> <span style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,0,0);"> </span> </td>
    </tr> 
    <tr><td>rgb(0,255,0)</td> 
        <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,255,0);"> </span> </td></tr>
    <tr><td>rgb(0,0,255)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,0,255);"> </span> </td></tr>
    </tbody>
</table> 
 
 
 
<table style="text-align:center;">
    <thead>
    <tr>
    <th>Code rgb</th> <th> couleur </th>
    </tr>
    </thead>
    
    <tbody>
    <tr><td>rgb(150,0,0)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(150,0,0);"> </span> </td>
    </tr>
    <tr><td>rgb(0,150,0)</td> 
    <td> <span  style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,150,0);"> </span> </td>
    </tr>
    <tr><td>rgb(0,0,150)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,0,150);"> </span> </td>
    </tr>
    <tr><td>rgb(255,0,255)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,0,255);"> </span> </td>
    </tr>
    <tr><td>rgb(0,255,255)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,255,255);"> </span> </td>
    </tr>
    <tr><td>rgb( 255,255,0)</td> 
    <td> <span   style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,255,0);"> </span> </td>
    </tr>
    </tbody>
</table>


Vous pouvez également facilement parcourir les codages RGB en cliquant sur l'élément de formulaire suivant :
<form>  <input type="color"> </form>

Firefox fait apparaître les deux autres codages usuels des couleurs lorsque 
vous choisissez une couleur dans cet élément de formulaire. 




## Le codage hexadécimal


### Quelques correspondances RGB-Hexadécimal


<table style="text-align:center;">
    <caption> Exemples de couleurs</caption>
    <thead>
    <tr>
    <th>Code rgb</th> <th> couleur </th> <th>Code hexadécimal</th>
    </tr>
    </thead>
    
    <tbody>
    <tr><td>rgb(255,0,0)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,0,0);"> </span> </td>
    <td> #ff0000 </td>
    </tr>
    <tr><td>rgb(150,0,0)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(150,0,0);"> </span> </td>
    <td> #960000 </td>
    </tr>
    <tr><td>rgb(0,255,0)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,255,0);"> </span> </td>
    <td> #00ff00 </td>
    </tr>
    <tr><td>rgb(0,150,0)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,150,0);"> </span> </td>
    <td> #009600 </td></tr>
    <tr><td>rgb(0,0,255)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,0,255);"> </span> </td>
    <td> #0000ff </td></tr>
    <tr><td>rgb(0,0,150)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,0,150);"> </span> </td>
    <td> #000096 </td></tr>
    <tr><td>rgb(255,0,255)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,0,255);"> </span> </td>
    <td> #ff00ff </td></tr>
    <tr><td>rgb(0,255,255)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(0,255,255);"> </span> </td>
    <td> #00ffff </td></tr>
    <tr><td>rgb( 255,255,0)</td> 
    <td> <span class="couleur" style="display: inline-block; width: 40px; height: 20px;background-color:rgb(255,255,0);"> </span> </td>
    <td> #ffff00 </td></tr>
    </tbody>
</table>


### Principe du codage hexadécimal

Le codage hexadécimal d'une couleur est constitué de six 'chiffres' 
pris parmi les chiffres 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f.

Les chiffres a, b, c, d, e, f représentent 10, 11, 12, 13, 14, 15.


<table>
<tr><td>a</td><td>b</td><td>c</td><td>d</td><td>e</td><td>f</td></tr>
<tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td></tr>
</table>

### Du codage hexadécimal au codage rgb

Pour passer du codage hexadécimal au codage rgb, on regroupe les chiffres du codage hexadécimal par deux. 
Le groupe de deux lettres
'xy' représente un entier en base 16. 
Il s'agit de le traduire en base 10. L'entier en base 10 correspondant est 16x+y.



<table>
    <caption> De l'hexadécimal au rgb </caption>
    <thead>
    <tr>
    <th>Code hexadécimal</th>  <th>opération</th>  <th>Code rgb </th>
    </tr>
    </thead>
    
    <tbody>
     <tr><td>#fa12b5</td>
     <td>  fa : \( f \times 16 + a = 15 \times 16 + 10 = 250 \),<br>
      12 : \( 1\times 16+2= 18\),<br>
       b5 : \( b\times 16+ 5 =11\times 16+ 5 =181\)</td>
     <td>rgb(250,18,181)</td></tr>
     <tr><td>#0d50aa</td>
     <td>  0d : \( 0 \times 16 + d = 0 \times 16 + 13 = 13 \),<br>
      50 : \( 5\times 16+0= 80\),<br>
       aa : \( a\times 16+ a =10\times 16+ 10=170\)</td>
     <td>rgb(13,80,170)</td></tr>
    </tbody>
</table>



### Du codage rgb au codage hexadécimal

Pour écrire un entier (écrit en base 10, 
c'est à dire la base usuelle) compris entre 0 et 255 en hexadécimal, 
on pose la division (entière) de l'entier par 16, le quotient 
et le reste donnent les chiffres de l'écriture hexadécimale.


<table>
    <caption> Du rgb à l'hexadécimal   </caption>
    <thead>
    <tr>
    <th>Code rgb</th>  <th>division</th>  <th>Code hexadécimal </th>
    </tr>
    </thead>
    
    <tbody>
     <tr><td>rgb(186, 213, 14) </td>
     <td>  186 : \( 186 = 11\times 16 + 10 = b \times 16 + a  \) &rarr; ba,<br>
      213 : \( 213= 13\times 16+5= d\times 16+5\) &rarr; d5,<br>
       14 : \( 14 = 0\times 16+ 14 =0\times 16+ e\) &rarr; 0e</td>
     <td>#bad50e</td></tr>
     <tr><td>rgb(255,0,16)</td>
     <td>  255 : \( 255 = 15 \times 16 + 15 = f \times 16 + f  \) &rarr; ff,<br>
       0 : \(  0 = 0\times 16+0\) &rarr;  00,<br>
       16 : \( 16= 1\times 16+ 0 \) &rarr; 10</td>
     <td>#ff0010</td></tr>
    </tbody>
</table>



## Exercice 1

+ Donner le code hexadécimal du code couleur rgb(92, 164, 211).


??? solution  "Réponse"

    On pose la division de 92 par 16: $92 = 5\times 16 +12$.  
    D'où les deux premiers chiffres en hexa : 5c.  

    On pose la division de 164 par 16: $164 = 10\times 16 +4$.
    D'où les deux  chiffres du milieu en hexa : a4. 
 
    On pose la division de 211 par 16: $211 = 13\times 16 +3$.
    D'où les deux derniers chiffres en hexa : d3.  

    Le code en hexadécimal : #5ca4d3.
    
    
+ Donner le code rgb du code couleur hexadécimal #ca9107.

 
??? solution  "Réponse"

    ca :  $c \times 16 +a = 12\times 16 + 10 = 202$. La composante rouge vaut 202. 
    
    91 : $9\times 16 + 1 = 145$. La composante verte vaut 145. 
    
    07 : $0\times 16 + 7 = 7$. La composante bleue vaut 7. 
    
    Le code rgb : rgb(202,145,7). 
    
    
## Exercice 2

Les éléments html sont tous inscrits dans une boîte rectangulaire. 
Ces boîtes rectangulaires peuvent être mises en évidence à l'aide de couleur de fond 
avec la propriété CSS `background-color`.

Le texte d'un paragraphe est lui mis en couleur avec la propriété CSS `color`.

Dans l'exemple de code suivant, les codes couleurs sont donnés en RGB. 
Modifiez les pour les obtenir en hexadécimal. 


Le code CSS est le code qui se situe entre les balises `<style>` `</style>`.

Le code:

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>  De RGB vers l'hexa </title>
        
        <style>
        p{
        	background-color : rgb(100, 0,200); /* couleur de fond des paragraphes */
        	color : rgb(0, 150, 50); /* couleur du texte des paragraphes */
        	font-weight: bold; /* texte mis en gras */
        }
        </style>
    </head>
    <body>
        
        <p>
            Un paragraphe en langage html est délimité par des balises p.
            Ce paragraphe montre qu'il ne faut pas choisir les couleurs n'importe comment!
        </p>
	  
        
    </body>
</html>
```

Vous testerez bien sûr le résultat en copiant-collant ce code dans un fichier .html, à ouvrir ensuite
avec un navigateur.

??? solution "Réponse"


    ### rgb(100, 0,200)



    On pose la division de 100 par 16: $100 = 6\times 16 +4$.
    D'où les deux  premiers chiffres en hexa : 64. 

    On pose la division de 0 par 16: $0 = 0\times 16 +0$.
    D'où les deux   chiffres intermédiaires en hexa : 00. 

    On pose la division de 200 par 16: $200  = 12\times 16 +8$.
    D'où les deux derniers chiffres en hexa : c8. 

    Le code en hexadécimal : #6400c8.


     
      
    ### rgb(0, 150, 50) 

    On pose la division de 0 par 16: $0  = 0\times 16 +0$.
    D'où les deux premiers chiffres en hexa : 00. 

    On pose la division de 150 par 16: $150 = 9\times 16 +6$.
    D'où les deux  chiffres du milieu en hexa : 96. 

    On pose la division de 50 par 16: $50 = 3\times 16 +2$.
    D'où les deux derniers chiffres en hexa : 32. 

    Le code en hexadécimal : #009632.


    ### Le code 

    ```html
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>  De RGB vers l'hexa </title>
            
            <style>
            p{
                background-color : #6400c8; /* couleur de fond des paragraphes */
                color : #009632; /* couleur du texte des paragraphes */
                font-weight: bold; /* texte mis en gras */
            }
            </style>
        </head>
        <body>
            
            <p>
                Un paragraphe en langage html est délimité par des balises p.
                Ce paragraphe montre qu'il ne faut pas choisir les couleurs n'importe comment!
            </p>
            
        </body>
    </html>
    ```



    <iframe id="couleurs a"
    title="Couleurs"
    width="100%"
    height="100"
    src="fichiersHTML/couleurs_a.html">
    </iframe>

## Exercice 3


Les éléments html sont tous inscrits dans une boîte rectangulaire. 
Ces boîtes rectangulaires peuvent être mises 
en évidence à l'aide de couleur de bordure avec la propriété CSS `border-color`.



Dans l'exemple de code suivant, les codes couleurs sont donnés en hexadécimal. Modifiez les pour les obtenir en rgb.

Le code CSS est le code qui se situe entre les balises `<style>` et `</style>`.


Le code:


```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>  De l'hexa vers rgb </title>
        
        <style>
        div{
        	 border-width : 3px; /* épaisseur du trait de bordure */
        	 border-style : dotted; /* trait de bordure en pointillés */
        	 border-color : #ffaa00; /* couleur de bordure */
        	 color : #00cb00; /* couleur du texte */
        }
        </style>
    </head>
    <body>
        
		 <div> Une division est une boîte de type block par défaut. </div>
	  
        
    </body>
</html>
```


??? solution "Solution"


    ### #ffaa00


    ff =  $15 \times  16 +15$ = 255.

    aa = $10\times 16 +10$ = 170. 

    00 = $0 \times 16 +10$ = 0. 

    Le code RGB est donc rgb(255, 170, 0). 
    
    Essayez de modifier le code dans le cadre de l'énoncé et vérifier que la couleur est inchangée.

      
    ### #00cb00 

    cb = $12 \times 16 + 11$ = 203.
    Le code RGB est donc rgb(0, 203, 0). 

    Essayez de modifier le code dans le cadre de l'énoncé et vérifier que la couleur est inchangée.

      
     
    ### Le code modifié




    ```html
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>  De l'hexa vers rgb </title>
            
            <style>
            div{
                 border-width : 3px; /* épaisseur du trait de bordure */
                 border-style : dotted; /* trait de bordure en pointillés */
                 border-color : rgb(255, 170, 0); /* couleur de bordure */
                 color : rgb(0, 203, 0); /* couleur du texte */
            }
            </style>
        </head>
        <body>
            
             <div> Une division est une boîte de type block par défaut. </div>
          
            
        </body>
    </html>
    ```

    <iframe id="couleurs b"
    title="Couleurs"
    width="100%"
    height="100"
    src="fichiersHTML/couleurs_b.html">
    </iframe>
