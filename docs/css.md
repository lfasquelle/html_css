# Le css

Le langage css (Cascading Style Sheets: feuilles de style en cascade) sert à mettre en forme la page: couleurs, taille du texte, positions relatives de certains blocs...

Comme pour le langage html, nous ne ferons qu'effleurer ce langage. Le principe est de vous donner des points de 
repère pour décrypter des pages html existantes.

Les bases données dans les pages de cours doivent être maîtrisées. 


## Couleurs

Il existe plusieurs façons de coder les couleurs en CSS. Nous avons vu les codes rgb et hexadécimal. 
Il est possible, pour certaines couleurs, d'utiliser simplement le nom (red, blue, yellow, purple... vous
trouverez facilement sur la toile des listes de noms de couleur acceptés par les navigateurs).


On peut colorer les fonds des boîtes (rappelons que tout élément html s'inscrit dans un rectangle, qu'on appellera 
souvent boîte), leurs bordures, le texte...

### Un exemple

+ Entrer le code suivant dans une page html et visualiser avec un navigateur. 
+ Chercher sur le web les noms d'autres couleurs, modifier les couleurs utilisées ici.


```html
<!DOCTYPE html>
<html lang="fr">

 
<head>
	<meta charset="utf-8">
	<title> je suis le titre  </title>
    
 
    <style>
    /* code css  */
    body{ /* pour que nos blocs n'occupent que 70% partie de la largeur d'écran */
        width: 70%;
        margin:0 auto;
    }
    div{
        border: 8px ridge green; /* les div seront encadrés en vert */
        margin-bottom: 5px; /* 5 pixels de marge après un div */
        padding: 3px; /* un peu de marge entre le texte et la bordure.*/
    }
    p{
        border: 1px dashed red; /* les paragraphes seront encadrés en rouge */
        color: magenta; /* le texte sera de couleur magenta */
        background-color: yellow; /* le fond de chaque paragraphe sera jaune */
        text-align: center; /* on centre, en largeur, le texte dans le paragraphe. */
    }
    
    h2{
        color: blue; /* les titres de niveau 2 en bleu */
    }
    </style>
</head>
 
<body>
    
<h1>Des citations</h1>
    
<div>
    <h2>Du Raymond devos</h2>

    <p>
        Je connais un critique qui est en même temps auteur... 
        ce qui le met en tant qu'auteur dans une situation critique !
    </p>

    <p>
        Une fois rien, c'est rien ; deux fois rien, ce n'est pas beaucoup, 
        mais pour trois fois rien, on peut déjà s'acheter quelque chose, et pour pas cher. 
    </p>

    <p>
        Mon pied droit est jaloux de mon pied gauche. 
        Quand l'un avance, l'autre veut le dépasser. Et moi, comme un imbécile, je marche!
    </p>
</div>



<div>
    <h2>Du Philippe Geluck</h2>

    <p>
        Boire du café empêche de dormir. 
        Par contre, dormir empêche de boire du café.
    </p>

    <p>
        Il est plus facile de jouer au mikado avec des spaghettis crus qu'avec des spaghettis cuits.
    </p>

    <p>
        Les imbéciles pensent que tous les noirs se ressemblent. 
        Je connais un noir qui trouve, lui, que tous les imbéciles se ressemblent.
    </p>
</div>
 
</body>
</html>
```


??? solution "Le rendu"

    La [page html correspondante](fichiersHTML/couleurs1.html).

## Bordures

Les bordures sont les tours des boîtes html, elles permettent donc de délimiter les éléments html.

 
 
Nous avons vu des exemples précédemment. La syntaxe d'une règle css concernant la propriété border:

```
nom de l'élément html{
    border: épaisseur style couleur; 
}
```

!!! note
    Vous trouverez les différents style de tracé de bordure 
    sur [cette page](https://www.w3schools.com/css/css_border.asp) 
    de w3schools.



## Taille de fonte

Par défaut, certains éléments sont représentés avec une taille de police plus grande que pour d'autres éléments.
C'est le cas des titres.

Cela est dû au fait que les navigateurs utilisent tous une feuille de style css par défaut.

### Un exemple

L'exemple ci-dessous montre comment agir sur la taille de fonte.

+ Tester.
+ Faire quelques essais de modifications.


```html
<!DOCTYPE html>
<html lang="fr">

 
<head>
	<meta charset="utf-8">
	<title> Taille de fonte  </title>
    <style>
    html{font-size: 20px; /* 20px: taille de référence */}
    body{width:70%; margin:0 auto;}
    h1{font-size: 3rem; /* 3 fois 20px = 60px*/}
    h2{
        font-size: 2rem; /* 40px */
        color: green;
    }
    h3{font-size: 1.5rem; /* 30px */}
    p{ color: blue; 
        /* taille : 20px = taille de référence */
    }
    </style>
</head>

<body>
    
    
    <h1>La taille</h1>
 
<div>
    <h2> Un élément div</h2>

    <div>
        <h3>Un div dans le div</h3>
        <p> Coucou</p>
        <p> Bouh! </p>
        <p> Zouh! </p>
    </div>

</div>


<div>
    <h2> Un second élément div</h2>

    <div>
        <h3>Un div dans le div</h3>
        <p> Zaza</p>
        <p> Baba </p>
    </div>

</div>


</body>
</html>
```


 
