# QCM 



!!! note  
    Les QCM sont là pour vous aider à retenir l'essentiel.
    Pour ce thème particulier html-css qu'on ne peut que survoler en NSI, 
    il est possible que vous ayez une recherche
    complémentaire à faire pour répondre à certaines questions.  
    Vous veillerez toutefois à retenir les questions-réponses de cette page.
    
    
    
    
    
    

Trouvez la ou les bonnes réponses.


## QCM 1

Quelle  balise utilisera-t-on de préférence pour le titre principal d'une page html:

- [ ] `<h1>`
- [ ] `<head>`
- [ ] `<heading>`
- [ ] `<h6>`


 
??? solution "Réponse"

    - [X] `<h1>`
    - [ ] `<head>`
    - [ ] `<heading>`
    - [ ] `<h6>`

       
       
## QCM 2

Utiliser la balise `h1`  signifie que l'on veut que le titre
apparaisse  plus gros que si l'on utilisait la balise `h2` :

- [ ] vrai
- [ ] faux
 


 
??? solution "Réponse"

    - [ ] vrai
    - [X] faux
        
    On utilise h1 pour un titre de plus haut niveau (par exemple, on utilisera h1 pour le titre de la page
    et h2 pour les titres des éléments section de la page). Mais cela n'a aucune signification sur l'apparence des titres qui devra
    être réglé par css.  
    Les navigateurs utilisent un css par défaut qui affiche toujours les titres h1 
    plus gros que les titres h2, mais attention, c'est bien le css qui décide de cela, libre à vous d'inverser
    visuellement les ordres de taille. Ne confondez pas le rôle   avec l'aspect visuel.
       
       
       
       
       
## QCM 3  
 


Pour mettre en gras une partie de texte dans une page html:

- [ ] on utilise une balise html spéciale.
- [ ] on utilise une propriété css.


??? solution "Réponse" 


    - [ ] on utilise une balise html spéciale.
    - [X] on utilise une propriété css.
    
    Cette propriété est [font-weight](https://developer.mozilla.org/fr/docs/Web/CSS/font-weight).
    
    Vous obtiendrez parfois sur certains sites une indication sur une balise pour mettre en gras.
    Cela est à éviter. Une telle indication peut être un reste de cours html datant d'il y a déjà 
    plusieurs années (à une époque, la séparation entre forme et contenu n'était 
    toujours très bien marquée). Ce peut être aussi une confusion de la part de l'auteur
    de la phrase entre la mise en forme par défaut des navigateurs
    et le rôle de la balise.
    Par exemple l'usage de la balise 
    strong permet de délimiter un texte important. Cela peut se traduire par une mise en gras (c'est le choix 
    par défaut des navigateurs) mais ce n'est pas une obligation, on peut décider par exemple de surligner
    en vert tous les contenus des balises strong (à l'aide d'une règle css).
        
        
        
        
        
        
        
## QCM 4
Les commentaires en HTML commencent par `<!--` et finissent par `-->`.

- [ ] Vrai
- [ ] Faux

 
??? solution "Réponse"

    - [X] Vrai
    - [ ] Faux
        
        
        
        
        
## QCM 5


En HTML5, la balise `<header>` a le même rôle que la balise `<head>` ?

- [ ] Vrai
- [ ] Faux

 
??? solution "Réponse"

    - [ ] Vrai
    - [X] Faux

    La balise head délimite les métainformations de la page.  
    La balise header délimite des contenus introductifs (introduction de la page, introduction
    de sections, ... elle contient souvent les titres, éventuellement des logos...)
        
        
        
## QCM 6

On souhaite rédiger une page html avec des énoncés et des corrections. 

Pour cela, on va utiliser la balise `<div>`. 
Quel  est l'attribut de `<div>` qui permettra de différencier les énoncés des corrections ?

- [ ] l'attribut class=""
- [ ] l'attibut id=""
- [ ] la balise `<strong>`
- [ ] la balise `<p>`
 
??? solution "Réponse"

    - [X] l'attribut class=""
    - [ ] l'attibut id=""
    - [ ] la balise `<strong>`
    - [ ] la balise `<p>`
        
        
    On utilisera par exemple la valeur `enonce` pour les div d'énoncés : `<div class="enonce">`
    et la valeur `solution` pour les div solutions: `<div class="solution">`.
    
    On pourra ensuite déclarer des règles css pour ces deux classes pour distinguer visuellement
    ces éléments div.
        
        
        
        
## QCM 7
Quel est le doctype d'un document HTML5 ?


- [ ] `<!DOCTYPE html5>`
- [ ] `<!DOCTYPE html> `
- [ ] `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML5.0 Strict//EN">`
 
??? solution "Réponse"

    - [ ] `<!DOCTYPE html5>`
    - [X] `<!DOCTYPE html> `
    - [ ] `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML5.0 Strict//EN">`
        
        
        
        
        
        
        
## QCM 8
Pour créer un lien vers la page d'accueil de Wikipédia, on écrira...


- [ ] `<a target="http://wikipedia.org">Wikipédia</a>`
- [ ] `<a href="http://wikipedia.org">`
- [ ] `<a href="http://wikipedia.org">Wikipédia</a>`
 
 
 
 
 
 
??? solution "Réponse"


    - [ ] `<a target="http://wikipedia.org">Wikipédia</a>`
    - [ ] `<a href="http://wikipedia.org">`
    - [X] `<a href="http://wikipedia.org">Wikipédia</a>`
    
    
    
    
        
## QCM 9
Quels éléments sont nécessaires pour créer une liste dont les items ne sont pas numérotés ?

  
- [ ] ul et li
- [ ] ol et li
- [ ] ul et ol
 
??? solution "Réponse"



    - [X] ul et li
    - [ ] ol et li
    - [ ] ul et ol

    Vous pouvez voir un exemple [sur cette page MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Element/ul).
        
        
        
## QCM 10
Laquelle de ces syntaxes est correcte pour embarquer une image dans la page html?


- [ ] `<img>src="mon-image.jpg" alt="Une image"</img>`
- [ ] `<img src="mon-image.jpg" alt="Une image">`
- [ ] `<img href="mon-image.jpg" alt="Une image">`
 
??? solution "R éponse"


    - [ ] `<img>src="mon-image.jpg" alt="Une image"</img>`
    - [X] `<img src="mon-image.jpg" alt="Une image">`
    - [ ] `<img href="mon-image.jpg" alt="Une image">`
        
    Une illustration sur [MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Element/img).
        
        
        
        
## QCM 11

Le rôle du CSS est de:

  
- [ ] Mettre en forme les éléments html d'une page.
- [ ] Définir des formulaires.
- [ ] Créer des sites e-commerce.
 
??? solution "Réponse"

    - [X] Mettre en forme les éléments html d'une page.
    - [ ] Définir des formulaires.
    - [ ] Créer des sites e-commerce
    
    
    
    
    
    
## QCM 12


Laquelle de ces pratiques doit être exceptionnelle (c'est à dire très rarement utilisée):

  
- [ ] placer le css dans un élément HTML style à l'intérieur de la balise head de la page.
- [ ] placer le css dans un attribut style de chaque élément html dans la page.
- [ ] placer le css dans un fichier ".css" séparé 
 
 
 
??? solution "Réponse"


 
    - [ ] placer le css dans un élément HTML style à l'intérieur de la balise head de la page.
    - [X] placer le css dans un attribut style de chaque élément html dans la page.
    - [ ] placer le css dans un fichier ".css" séparé 
     
    Illustration de l'attribut style sur un élément html [sur cette page MDN](https://developer.mozilla.org/fr/docs/Web/HTML/Attributs_universels/style).
    Si cela est déconseillé, c'est uniquement en prévision du développement et de la maintenance de votre site:
    dès que ce style est utilisé plus d'une fois, vous devez le mettre "en facteur" soit dans le head de la page 
    (s'il est certain qu'il ne sera utilisé que dans cette page), soit dans un fichier séparé. Ainsi toute
    modification ultérieure ne vous demandera d'agir qu'à un seul endroit: c'est moins long et moins propice
    aux erreurs et oublis.
        
        
        
        
        
        
        
        
## QCM 13
Que veut dire HTML ?

  
- [ ] HyperText Make Language
- [ ] HyperText Markup Language
- [ ] HyperText Make Link
- [ ] HyperText Markup Link
 
??? solution "Réponse"

    - [ ] HyperText Make Language
    - [X] HyperText Markup Language
    - [ ] HyperText Make Link
    - [ ] HyperText Markup Link











## QCM 14



Que veut dire CSS ?
  
 
- [ ] Cascading Style Shape 
- [ ] Cascade Science Sheets
- [ ] Cascading Style Sheets
 
??? solution "Réponse"



     
    - [ ] Cascading Style Shape 
    - [ ] Cascade Science Sheets
    - [X] Cascading Style Sheets (feuilles de style en cascade) 
    
    
 
## QCM 15

Le code couleur `rgb(35, 16, 7)` se traduit en hexadécimal par:


- [ ] `#351607`
- [ ] `#231007`
- [ ] `#320170`
- [ ] `#071023`


??? solution "Réponse"



    - [ ] `#351607`
    - [X] `#231007`
    - [ ] `#320170`
    - [ ] `#071023`




  
## QCM 16

Le code couleur `#aa1100` se traduit en RGB par:


- [ ] `rgb(170, 17, 0)`
- [ ] `rgb(20, 11, 0)`
- [ ] `rgb(100, 17, 0)`
- [ ] `rgb( 0, 17, 170)`


??? solution "Réponse"



    
    - [X] `rgb(170, 17, 0)`
    - [ ] `rgb(20, 11, 0)`
    - [ ] `rgb(100, 17, 0)`
    - [ ] `rgb( 0, 17, 170)`
    
    
## QCM 17

La structure correcte pour une page html est:


- [ ] 
```html
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <title> Le titre de la page  </title>
    </head>
<html lang="fr">
    <body>
     ICI LE CONTENU DE LA PAGE
    </body>
</html>
```




- [ ] 
```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> Le titre de la page  </title>
    </head>

    <body>
     ICI LE CONTENU DE LA PAGE
    </body>
</html>
```


- [ ] 
```html
<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <title> Le titre de la page  </title>
    </head>
<body>
    <html lang="fr">
     ICI LE CONTENU DE LA PAGE
    </html>
</body>
```



- [ ] 
```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> Le titre de la page  </title>
    </head>

    <body>
     ICI LE CONTENU DE LA PAGE
</html>
</body>
```


??? solution "Réponse"



    
    - [ ] 
    ```html
    <!DOCTYPE html>
        <head>
            <meta charset="utf-8">
            <title> Le titre de la page  </title>
        </head>
    <html lang="fr">
        <body>
         ICI LE CONTENU DE LA PAGE
        </body>
    </html>
    ```




    - [X] 
    ```html
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title> Le titre de la page  </title>
        </head>

        <body>
         ICI LE CONTENU DE LA PAGE
        </body>
    </html>
    ```


    - [ ] 
    ```html
    <!DOCTYPE html>
        <head>
            <meta charset="utf-8">
            <title> Le titre de la page  </title>
        </head>
    <body>
        <html lang="fr">
         ICI LE CONTENU DE LA PAGE
        </html>
    </body>
    ```



    - [ ] 
    ```html
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title> Le titre de la page  </title>
        </head>

        <body>
         ICI LE CONTENU DE LA PAGE
    </html>
    </body>
    ```



## QCM 18

Avec la règle css suivante:

```css
p.bli{
    color: red;
}
```

- [ ] le texte de tous les paragraphes sera rouge.
- [ ] seul le texte des paragraphes de classe bli sera rouge.
- [ ] seul le texte du paragraphe  d'identifiant bli sera rouge.
- [ ] la couleur de fond des paragraphes de classe bli sera rouge.


??? solution "Réponse"

    - [ ] le texte de tous les paragraphes sera rouge.
    - [X] seul le texte des paragraphes de classe bli sera rouge.
    - [ ] seul le texte du paragraphe  d'identifiant bli sera rouge.
    - [ ] la couleur de fond des paragraphes de classe bli sera rouge.

    
