
# Création d'une page web

Tout le monde connaît Bill Gates, Steve Jobs ou Mark Zuckerberg.

Beaucoup moins célèbres mais bien plus importants, 
voici une liste  de quelques grands informaticiens:

+ <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/220px-JohnvonNeumann-LosAlamos.jpg">John Von Neumann 1903-1957</a>
+ <a href="http://www.csupomona.edu/~plin/inventors/images/hoppergrace_big.jpg">Grace Hopper 1906-1992</a>
+ <a href="http://a3.idata.over-blog.com/300x347/0/02/64/66/ALBUM---2/ALBUM_3/alan_turing.jpg">Alan Turing 1912-1954</a>
+ <a href="http://idelways.developpez.com/news/images/dennis_ritchie.jpg">Dennis Ritchie 1941-2011</a>
+ <a href="http://lh6.ggpht.com/_znke0ocVW6k/TGLVBIOD08I/AAAAAAAAAHE/a6ckD02690s/stallman.jpg?imgmax=800">Richard Stallman 1953-</a>
+ <a href="http://rdonfack.developpez.com/tim-berners-lee.jpg">Tim Berners-Lee 1955-</a>
+ <a href="http://www.journaldunet.com/solutions/ssii/selection/10-grandes-figures-de-l-open-source/image/linus-torvalds-osdl-492878.jpg">Linus Torvalds 1969-</a>

Avec un éditeur de texte, geany par exemple, écrire le code HTML d'une page Web qui ressemble à ceci :

![](exercice2/informaticiens.png)

+ On fera en sorte que les images soient toutes de la même hauteur (attribut height).
+ Les légendes des images seront obtenues à l'aide des balises 
[figure et figcaption](https://developer.mozilla.org/fr/docs/Web/HTML/Element/figure).
+ On   utilisera   la propriété css grid pour la mise en place des images.
+  Avec le style CSS, modifiez à votre goût la couleur de fond de la page, la police, 
la couleur du texte, etc.
+  Télécharger les photos : <a href="exercice2/photos.zip">dossier photos.zip</a>.


!!! important
    N'oubliez pas de tester la validité de votre page html 
    avec le [w3 validator](https://validator.w3.org/#validate_by_upload).
    Ainsi que votre feuille de style avec le [w3 css validator](https://jigsaw.w3.org/css-validator/validator.html.en#validate_by_upload).
    

??? solution "Aide"

    Un [premier fichier](exercice2/informatik.html). 
    Un certain nombre de balises sont en place. Compléter les balises et le css.
    
    
??? solution "Une réponse possible"
    [Un exemple avec grid](exercice2/informatik2.html).
