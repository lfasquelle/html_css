# Reproduire une page web


En supplément des pages de cours précédentes, vous n'hésiterez pas à utiliser les [liens de cette page](surlatoile.md):
il faut connaître les bases pour pouvoir démarrer, ensuite ce que l'on vous demande est de savoir chercher
les informations dans les documents présents et sur le web. 



## Exercice 1


Votre objectif va être de reproduire [cette page html](exercice1/historik.html).

[Ce fichier html](exercice1/brut.html) contient le texte mais presque toutes les balises ont été supprimées.
Téléchargez le: c'est ce fichier que vous devez compléter.

[Ce fichier css](exercice1/style.css) contient les règles css utilisées.

[Ce dossier](exercice1/photos.zip) contient les quatre photos à utiliser.


En complétant les balises de [ce fichier html](exercice1/brut.html), 
vous devez tenter de reproduire [la page html](exercice1/historik.html) 
(sans modifier le css): votre objectif est de comprendre les règles css
déclarées 
et de retrouver quelles balises sont utilisées pour chaque composante de la page html.


Regardez le moins possible la solution. L'objectif est que vous fassiez des essais, 
que vous vous habituiez aux balises et aux effets
des règles css. 

N'oubliez pas de transformer en lien (sur la page wikipedia source) le nom de la personne citée en chaque début 
de paragraphe.


!!! important
    N'oubliez pas de tester la validité de votre page html avec le [w3 validator](https://validator.w3.org/#validate_by_upload).
    


??? solution "Solution"

    [Le fichier html](exercice1/historik.html) à reproduire contient la solution (clic droit/ code source).
    Ou plutôt une solution: il est tout à fait possible que vous ayez obtenu une page semblable avec 
    quelques choix un peu différents.



## Exercice 2


Ajouter les balises nécessaires et les règles css nécessaires pour ajouter une colonne de liens à
gauche.

Le résultat à obtenir est [celui-ci](exercice1/historik2.html). Vous aurez les adresses à cibler (il s'agit
de pages concernant l'histoire de l'informatique) en cliquant sur les liens!


Là encore, l'objectif n'est pas de regarder le code que l'on vous propose mais de tenter d'en écrire un vous-même
menant à un résultat semblable.
